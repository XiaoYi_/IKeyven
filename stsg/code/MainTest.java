package stsg.code;

import java.util.Scanner;

import org.ansj.splitWord.analysis.ToAnalysis;

import _Keyven.StanfordParser;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class MainTest {
	public static void main(String[] args) {
		ToAnalysis.parse("");
		StanfordParser.lp = LexicalizedParser
				.loadModel("lib/chinesePCFG.ser.gz");
		System.out.println("comp-user input:");

		Scanner input = new Scanner(System.in);

		ISTSG user = STSGFactory.stsgproducer(STSGType.COMPRESSION);
		String str1 = input.nextLine();
		user.synchronous_tree_substitution_grammar(str1);
		
		user = STSGFactory.stsgproducer(STSGType.MOVEMENT);
		String str2 = input.nextLine();
		user.synchronous_tree_substitution_grammar(str2);

		input.close();
	}
}
